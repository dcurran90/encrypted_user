# README #

This is a tool for encrypting and decrypting your user credentials for use in other scripts.
It uses these other python modules:

* argparse
* getpass
* pickle
* pexpect
* sagecipher

install these with:

	pip install -r requirements.txt

## How do I get set up? ##

	git clone git@bitbucket.org:schizoid90/encrypted_user.git

## How to use it ##

### Create encrypted details ###

	python -m launch -e ~/.credentials

It will ask for your username and password and save the result to .credentials in your home directory

### Using this in a script ###

	from credentials.decrypt import *
	creds = (show_details(credfile))

	username = creds[0]
	passwd = creds[1]

Examples exist in the examples directory for using as part of mysql.connect, and pexpect for ssh and telnet
