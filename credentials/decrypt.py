import pickle
import sys
from sagecipher import *


def show_details(credfile):
    if credfile:
        print(credfile)
        creds = pickle.loads(decrypt_string(open(credfile, 'r').read()))
        return creds
    else:
        print("Can't find credentials %s, exiting" % (credfile,))

if __name__ == '__main__':
    # All supported command line arguments
    print(show_details(sys.argv[1]))