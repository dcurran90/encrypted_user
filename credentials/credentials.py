#!/usr/bin/env python
import pickle
from sagecipher import *

def save_credentials(data, credfile):
    with open(credfile, 'w') as f:
        f.write(encrypt_string(pickle.dumps(data)))

if __name__ == '__main__':
    if credfile:
        save_credentials([user, password], credfile)
    else:
        print("Please supply output file")