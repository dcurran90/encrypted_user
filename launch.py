import getpass
import argparse
from credentials.credentials import *
from credentials.decrypt import *
from examples.ssh import *
#from examples.mysql import *
from examples.telnet import *

"""[summary]
This will either encrypt or decrypt your credntials

[description]
Pass a filename with -e to encrypt a username and password
Use -d to decrypt the credentials in a file
"""

if __name__ == '__main__':
    ## All supported command line arguments
    parser = argparse.ArgumentParser(
        prog="python launch.py",
        description="Used to encrypt and decrypt user deatils",
    )
    parser.add_argument(
        '-e',
        dest='encrypt',
        help='file to hold credentials encryption',
    )
    parser.add_argument(
        '-d',
        dest='decrypt',
        help='file containing encrypted details for decryption',
    )
    parser.add_argument(
        '-s',
        dest='ssh',
        help='run the ssh function',
    )
    parser.add_argument(
        '-m',
        dest='mysql',
        help='run the mysql function',
    )
    parser.add_argument(
        '-t',
        dest='telnet',
        help='run the telnet function',
    )

    args = parser.parse_args()

    if args.encrypt:
        credfile = args.encrypt
        user = raw_input("Username: ")
        passwd = getpass.getpass(prompt="Password: ")
        save_credentials([user, passwd], credfile)
    elif args.decrypt:
        credfile = args.decrypt
        show_details(credfile)
    elif args.ssh:
        credfile = args.ssh
        ssh(credfile)
    elif args.telnet:
        credfile = args.telnet
        telnet(credfile)
    elif args.mysql:
        credfile = args.mysql
        mysql(credfile)
