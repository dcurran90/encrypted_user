import pexpect
from credentials.decrypt import *

def ssh(credfile):
    creds = (show_details(credfile))

    username = creds[0]
    passwd = creds[1]
    print("""username - %s
    password - %s""" % (username, passwd,))
    try:
        connect = pexpect.spawn('ssh %s@localhost' % (username,))
        s = connect.expect(['Connection refused','[P|p]assword',pexpect.TIMEOUT,'Connection closed by remote host'])
        
        if s == 0:
            print("connection refused")
        if s == 1:
            connect.sendline(passwd)
            connect.expect('[.*]')
            connect.sendline('echo "test" > ~/test.txt')
            connect.sendline('logout')
        if s == 3:
            print("Connection timed out")
        if s == 4:
            print("Connection closed by remote host")
    except Exception as e:
        print(e)

if __name__ == '__main__':
    ssh(credfile)