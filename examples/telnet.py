import pexpect
from credentials.decrypt import *

def telnet(credfile):
    creds = (show_details(credfile))

    username = creds[0]
    passwd = creds[1]

    try:
        connect = pexpect.spawn('telnet localhost');
        t = connect.expect(['Connection refused','Username:',pexpect.EOF]);

        if t == 0:
            print("connection refused")
        if t == 1:
            connect.sendline(username);
            connect.expect('Password:');
            connect.sendline(passwd);
            connect.expect('#', '>');
            connect.sendline('logout');
        if t == 2:
            print("Error")
    except Exception as e:
        print(e)


if __name__ == '__main__':
    telnet(credfile)

